from typing import ClassVar


class GameWordsProcessor:
    def __init__(self):
        self.words: list[str] = []
        self.last_char: str = ''

    def add_next_word(self, word: str) -> str:
        """
        add_next_word('example') -> 'e'
        """

        if (
                self.last_char
                and (current_last_character := word[0]) != self.last_char
        ):
            raise ValueError(f'Word begin with wrong character: {current_last_character}')

        if (word := word.lower()) in self.words:
            raise ValueError(f'Word is already exist:{word}')
        self.words.append(word)
        self.last_char = word[-1]
        return self.last_char


class GameEngine:
    _EXIT_TEXT: ClassVar[str] = '1'

    def __init__(self):
        self._game_words_processor = GameWordsProcessor()

    @property
    def last_char(self) -> str:
        return self._game_words_processor.last_char

    def _handle_input(self) -> str:
        prompt_message = 'Enter word.'
        if self.last_char:
            prompt_message = f'{prompt_message} It should start from the indicated letter "{self.last_char}".'

        print(prompt_message)

        return input(":")

    def run(self) -> None:
        start_message = """Welcome to the game:
        "Words
        To exit press "Enter"
        """
        is_continue_game = True
        while is_continue_game:

            while True:
                # [handle_input]-[BEGIN]
                input_text = self._handle_input()
                # [handle_input]-[END]

                # [handle_exit]-[BEGIN]
                if not input_text:
                    input_for_exit: str = input(f"Are you sure? Press {self._EXIT_TEXT} to exit ")
                    if input_for_exit == self._EXIT_TEXT:
                        is_continue_game = False
                        break
                # [handle_exit]-[END]

                # [handle_word]-[START]
                word: str = f'{self.last_char}{input_text}'
                try:
                    self._game_words_processor.add_next_word(word=word)
                except ValueError as exception:
                    print(exception)
                else:
                    break
                # [handle_word]-[END]


def words_engine():
    ...


def main() -> None:
    """
    Сделать простую консольную локальную групповую игру.
    Т.е. чтобы можно было её запустить на одном устройстве и вместе играть 2-м и более людям. Пока что с консольным интерфейсом.
    Можно с минимальным управлением. Даже если всё управление будет заключаться в нажимании ENTER, то пойдёт.

    Слова".
    Возможно, указание количества игроков. (Возможно с именами).
    Запоминание истории слов. Чтобы нельзя было снова использовать то же самое.
    Подбор последней буквы и подстановка в качестве первой. Возможно со "списком исключённых букв" (ь,ъ,ы и может что-то ещё).
    Возможно, с поддержкой сохранения сессии. Чтобы можно было выйти и затем продолжить.

    """

    game_engine = GameEngine()
    game_engine.run()
    # game_engine = GameWordsProcessor()
    # letter = game_engine.add_next_word('Hello')
    # letter = game_engine.add_next_word('Hello')
    print()


if __name__ == '__main__':
    main()
